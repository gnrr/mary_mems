################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc.c \
../src/array_com.c \
../src/color_led.c \
../src/cr_startup_lpc11.c \
../src/fixedpoint.c \
../src/gpio.c \
../src/gpsrtc.c \
../src/i2c.c \
../src/key.c \
../src/led_array.c \
../src/main.c \
../src/mems.c \
../src/mems_data.c \
../src/oled.c \
../src/pff.c \
../src/pwm.c \
../src/sdcard.c \
../src/spi.c \
../src/systick.c \
../src/timer16.c \
../src/uart.c \
../src/utility.c 

OBJS += \
./src/adc.o \
./src/array_com.o \
./src/color_led.o \
./src/cr_startup_lpc11.o \
./src/fixedpoint.o \
./src/gpio.o \
./src/gpsrtc.o \
./src/i2c.o \
./src/key.o \
./src/led_array.o \
./src/main.o \
./src/mems.o \
./src/mems_data.o \
./src/oled.o \
./src/pff.o \
./src/pwm.o \
./src/sdcard.o \
./src/spi.o \
./src/systick.o \
./src/timer16.o \
./src/uart.o \
./src/utility.o 

C_DEPS += \
./src/adc.d \
./src/array_com.d \
./src/color_led.d \
./src/cr_startup_lpc11.d \
./src/fixedpoint.d \
./src/gpio.d \
./src/gpsrtc.d \
./src/i2c.d \
./src/key.d \
./src/led_array.d \
./src/main.d \
./src/mems.d \
./src/mems_data.d \
./src/oled.d \
./src/pff.d \
./src/pwm.d \
./src/sdcard.d \
./src/spi.d \
./src/systick.d \
./src/timer16.d \
./src/uart.d \
./src/utility.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__USE_CMSIS=CMSISv1p30_LPC11xx -DDEBUG -D__CODE_RED -D__NEWLIB__ -I"/Users/g/Dropbox/test/mary/CQ_LPC1114/workspace/CMSISv1p30_LPC11xx/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



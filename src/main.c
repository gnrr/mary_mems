//=========================================================
// LPC1114 Project
//=========================================================
// File Name : main.c
// Function  : Main Routine
//---------------------------------------------------------
// Rev.01 2010.08.01 Munetomo Maruyama
//---------------------------------------------------------
// Copyright (C) 2010-2011 Munetomo Maruyama
//=========================================================
// ---- License Information -------------------------------
// Anyone can FREELY use this code fully or partially
// under conditions shown below.
// 1. You may use this code only for individual purpose,
//    and educational purpose.
//    Do not use this code for business even if partially.
// 2. You can copy, modify and distribute this code.
// 3. You should remain this header text in your codes
//   including Copyright credit and License Information.
// 4. Your codes should inherit this license information.
//=========================================================
// ---- Patent Notice -------------------------------------
// I have not cared whether this system (hw + sw) causes
// infringement on the patent, copyright, trademark,
// or trade secret rights of others. You have all
// responsibilities for determining if your designs
// and products infringe on the intellectual property
// rights of others, when you use technical information
// included in this system for your business.
//=========================================================
// ---- Disclaimers ---------------------------------------
// The function and reliability of this system are not
// guaranteed. They may cause any damages to loss of
// properties, data, money, profits, life, or business.
// By adopting this system even partially, you assume
// all responsibility for its use.
//=========================================================

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "array_com.h"
#include "color_led.h"
#include "fixedpoint.h"
#include "mems.h"
#include "oled.h"
#include "systick.h"
#include "uart.h"

#include "mems_data.h"

//================
// Globals (Debug)
//================
extern volatile uint32_t gTxSync_Count;
extern volatile uint32_t gRxSync_Count;

void Draw_Border(uint32_t color)
{
    OLED_Fill_Rect(  0,   0, 127,   1, color);
    OLED_Fill_Rect(  0, 127, 127,   1, color);
    OLED_Fill_Rect(  0,   0,   1, 127, color);
    OLED_Fill_Rect(127,   0,   1, 127, color);
}

//
int main(void)
{
    int8_t mx, my, mz;
    mems_t mxa, mya, mza;
    int8_t roofp, roofp_old;

    //
    // Initialization
    //
    Init_SysTick();
    Init_Color_LED();
    Init_OLED();
    Init_MEMS();
    UARTInit(230400);

    mems_init_buffer(&mxa);
    mems_init_buffer(&mya);
    mems_init_buffer(&mza);

    roofp = roofp_old = 0;

    OLED_printf_Font(OLED_FONT_MEDIUM);

    //
    // Main Loop
    //
    while(1)
    {
        mx = MEMS_Get_X();
        my = MEMS_Get_Y();
        mz = MEMS_Get_Z();

        mems_push_buffer(&mxa, mx);
        mems_push_buffer(&mya, my);
        mems_push_buffer(&mza, mz);

        //
        // Draw MEMS Status
        //
        OLED_printf_Position(0, 0);

        OLED_printf("X %3d\n", (int32_t)mx);
        OLED_printf("Y %3d\n", (int32_t)my);
        OLED_printf("Z %3d\n", (int32_t)mz);

        roofp = mems_is_roof(&mxa, &mya, &mza);

        if ((roofp_old == 0) && (roofp == 1)) {
            roofp_old = roofp;
            Draw_Border(OLED_GRN);
            UARTSend_Byte('R');
        } else if((roofp_old == 1) && (roofp == 0)) {
            roofp_old = roofp;
            Draw_Border(OLED_BLK);
        }
    }
    return 0;
}

//=========================================================
// End of Program
//=========================================================

/*
 * mems_data.c
 *
 *  Created on: Oct 21, 2013
 *      Author: g
 */

#include <stdlib.h>
#include "mems_data.h"

#define MEMS_INIT_VALUE 127
#define MEMS_MAX_VALUE 125
#define MEMS_MIN_VALUE -125

#define MEMS_THRESH_LO 10
#define MEMS_THRESH_HI 40

void mems_init_buffer(mems_t *buf)
{
    for (int i=0; i<MEMS_BUF_SIZE; i++)
        buf->data[i] = MEMS_INIT_VALUE;
    buf->ptr = 0;
}

void mems_push_buffer(mems_t *buf, uint8_t value)
{
    buf->data[buf->ptr++] = value;
    if (buf->ptr >= MEMS_BUF_SIZE) buf->ptr = 0;
}

int8_t mems_get_average(mems_t *buf)
{
    int8_t val;
    int sum, count;

    sum = count = 0;
    for (int i=0; i<MEMS_BUF_SIZE; i++) {
        val = buf->data[i];
        if ((val <= MEMS_MIN_VALUE) || (MEMS_MAX_VALUE <= val)) continue;

        sum += val;
        count++;
    }

    return (int8_t)(sum / count);
}

int8_t mems_is_roof(mems_t *xbuf, mems_t *ybuf, mems_t *zbuf)
{
    int8_t x, y, z;

    x = mems_get_average(xbuf);
    y = mems_get_average(ybuf);
    z = mems_get_average(zbuf);

    if ((abs(x) < MEMS_THRESH_LO) && (abs(y) < MEMS_THRESH_LO) && (z > MEMS_THRESH_HI))
        return 1;
    else
        return 0;
}

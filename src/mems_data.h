/*
 * mems_data.h
 *
 *  Created on: Oct 21, 2013
 *      Author: g
 */

#ifndef MEMS_DATA_H_
#define MEMS_DATA_H_

#include "LPC11xx.h"

#define MEMS_BUF_SIZE 10

typedef struct {
    uint8_t ptr;
    int8_t data[MEMS_BUF_SIZE];
} mems_t;


void mems_init_buffer(mems_t *buf);
void mems_push_buffer(mems_t *buf, uint8_t value);
int8_t mems_get_average(mems_t *buf);
int8_t mems_is_roof(mems_t *xbuf, mems_t *ybuf, mems_t *zbuf);


#endif /* MEMS_DATA_H_ */
